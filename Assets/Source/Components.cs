﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public struct CreatureComp : IComponentData
{
    public Entity FoodTarget;
    public float Birth;
    public float Energy;
    public float MaxEnergy;
    public float AbsorptionRate;
    public float ConsumptionRate;
    public float IdleConsumptionRate;
    public float IdleThreshold;
    public float Speed;
    public float LifeSpan;
    public bool IsIdle;
    public bool IsDead;
}

public struct DeadCreature : IComponentData
{
    public float Birth;
    public float LifeSpan;
}

public struct FoodComp : IComponentData
{
    public float Energy;
    public bool IsEaten;
}