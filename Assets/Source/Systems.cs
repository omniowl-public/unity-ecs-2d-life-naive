﻿using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;

public class FoodManagementSystem : ComponentSystem
{
    private EntityManager m_EntityManager;
    private CommonResources resources;
    private EntityArchetype foodType;

    protected override void OnCreate()
    {
        base.OnCreateManager();
        m_EntityManager = World.Active.EntityManager;
        foodType = m_EntityManager.CreateArchetype(
                typeof(FoodComp),
                typeof(Translation),
                typeof(Rotation),
                typeof(LocalToWorld),
                typeof(Scale),
                typeof(RenderMesh));
        resources = Resources.Load<CommonResources>("CommonResources");
        MakeFood(resources.MaxFoodCount);
    }

    protected override void OnUpdate()
    {
        NativeArray<Entity> allFood = m_EntityManager.CreateEntityQuery(typeof(FoodComp)).ToEntityArray(Allocator.TempJob);
        NativeList<Entity> destroyList = new NativeList<Entity>(Allocator.Temp);
        FoodComp food;
        for (int index = 0; index < allFood.Length; index++)
        {
            Entity entity = allFood[index];
            food = m_EntityManager.GetComponentData<FoodComp>(entity);
            if (food.IsEaten == true)
            {
                destroyList.Add(entity);
            }
        }

        int foodDif = allFood.Length - destroyList.Length;
        int difference = resources.MaxFoodCount - foodDif;

        m_EntityManager.DestroyEntity(destroyList);
        allFood.Dispose();
        destroyList.Dispose();

        if (difference > 0)
        {
            MakeFood(difference);
        }
    }

    private void MakeFood(int count)
    {
        NativeArray<Entity> newFood = new NativeArray<Entity>(count, Allocator.Temp);
        m_EntityManager.CreateEntity(foodType, newFood);
        Unity.Mathematics.Random rnd = new Unity.Mathematics.Random();
        rnd.InitState((uint)DateTime.Today.Ticks);

        for (int index = 0; index < newFood.Length; index++)
        {
            Entity entity = newFood[index];
            m_EntityManager.SetComponentData(entity, new FoodComp()
            {
                Energy = rnd.NextFloat(1, 30),
                IsEaten = false
            });
            m_EntityManager.SetComponentData(entity, new Translation()
            {
                Value = new float3(rnd.NextFloat(resources.Boundaries.MinX, resources.Boundaries.MaxX), 0, rnd.NextFloat(resources.Boundaries.MinZ, resources.Boundaries.MaxZ))
            });
            m_EntityManager.SetComponentData(entity, new Rotation()
            {
                Value = quaternion.EulerXYZ(90, 0, 0)
            });
            m_EntityManager.SetComponentData(entity, new Scale()
            {
                Value = 5f
            });
            m_EntityManager.SetSharedComponentData(entity, new RenderMesh()
            {
                castShadows = ShadowCastingMode.Off,
                mesh = resources.FoodMesh,
                material = resources.FoodMaterial
            });
        }
        newFood.Dispose();
    }
}

[UpdateAfter(typeof(FoodManagementSystem))]
public class CreatureEnergyConsumptionSystem : ComponentSystem
{
    private EntityManager m_EntityManager;
    private float deltaTime;


    protected override void OnCreate()
    {
        base.OnCreateManager();
        m_EntityManager = World.Active.EntityManager;
    }

    protected override void OnUpdate()
    {
        deltaTime = Time.deltaTime;
        Entities.ForEach((ref CreatureComp creature) =>
        {
            if (creature.IsDead == false)
            {
                switch (creature.IsIdle)
                {
                    case true:
                        creature.Energy -= (creature.IdleConsumptionRate * deltaTime);
                        break;
                    case false:
                        creature.Energy -= (creature.ConsumptionRate * deltaTime);
                        break;
                }
                creature.LifeSpan = (Time.time - creature.Birth);
            }
            if (creature.Energy > 0 && creature.Energy < creature.IdleThreshold)
            {
                creature.IsIdle = false;
            }
            else if (creature.Energy <= 0)
            {
                creature.IsDead = true;
            }
        });
    }
}

[UpdateAfter(typeof(CreatureEnergyConsumptionSystem))]
public class CreatureDeathSystem : ComponentSystem
{
    private EntityManager m_EntityManager;
    private EntityArchetype deadArchetype;
    private CommonResources resources;

    protected override void OnCreate()
    {
        base.OnCreateManager();
        m_EntityManager = World.Active.EntityManager;
        deadArchetype = m_EntityManager.CreateArchetype(
            typeof(DeadCreature),
            typeof(Translation),
            typeof(Rotation),
            typeof(Scale),
            typeof(LocalToWorld),
            typeof(RenderMesh));
        resources = Resources.Load<CommonResources>("CommonResources");
    }

    protected override void OnUpdate()
    {
        NativeArray<Entity> allCreatures = GetEntityQuery(typeof(CreatureComp)).ToEntityArray(Allocator.TempJob);
        NativeList<Entity> deadCreatures = new NativeList<Entity>(Allocator.TempJob);
        CreatureComp creature;
        for (int index = 0; index < allCreatures.Length; index++)
        {
            Entity entity = allCreatures[index];
            creature = m_EntityManager.GetComponentData<CreatureComp>(entity);
            if (creature.IsDead == true)
            {
                deadCreatures.Add(entity);
            }
        }
        if (deadCreatures.Length > 0)
        {
            CreateDeadCreature(deadCreatures.ToArray());
            m_EntityManager.DestroyEntity(deadCreatures);
        }
        allCreatures.Dispose();
        deadCreatures.Dispose();
    }

    private void CreateDeadCreature(Entity[] creatures)
    {
        NativeArray<Entity> entityArray = new NativeArray<Entity>(creatures.Length, Allocator.Temp);
        m_EntityManager.CreateEntity(deadArchetype, entityArray);

        CreatureComp creatureComp;
        Translation creaturePos;
        float creatureScale;

        for (int index = 0; index < entityArray.Length; index++)
        {
            Entity deadCreatureEntity = entityArray[index];
            Entity creatureEntity = creatures[index];
            creatureComp = m_EntityManager.GetComponentData<CreatureComp>(creatureEntity);
            creaturePos = m_EntityManager.GetComponentData<Translation>(creatureEntity);
            creatureScale = m_EntityManager.GetComponentData<Scale>(creatureEntity).Value;

            m_EntityManager.SetComponentData(deadCreatureEntity, new DeadCreature
            {
                Birth = creatureComp.Birth,
                LifeSpan = creatureComp.LifeSpan
            });
            m_EntityManager.SetComponentData(deadCreatureEntity, new Translation
            {
                Value = creaturePos.Value
            });
            m_EntityManager.SetComponentData(deadCreatureEntity, new Rotation()
            {
                Value = quaternion.EulerXYZ(90, 0, 0)
            });
            m_EntityManager.SetComponentData(deadCreatureEntity, new Scale
            {
                Value = creatureScale
            });
            m_EntityManager.SetSharedComponentData(deadCreatureEntity, new RenderMesh
            {
                castShadows = ShadowCastingMode.Off,
                mesh = resources.DeadCreatureMesh,
                material = resources.DeadCreatureMaterial
            });
        }
        entityArray.Dispose();
    }
}

public class ConsumeFoodJobSystem : JobComponentSystem
{
    [BurstCompile]
    public struct ConsumeFoodJob : IJobForEachWithEntity<FoodComp>
    {
        [DeallocateOnJobCompletion]
        [ReadOnly] public NativeArray<Entity> creatureEntities;
        [ReadOnly] public ComponentDataFromEntity<Translation> allTranslations;
        [ReadOnly] public ComponentDataFromEntity<Scale> allScales;
        [NativeDisableParallelForRestriction]
        public ComponentDataFromEntity<CreatureComp> allCreatureComps;

        public void Execute(Entity foodEntity, int index, ref FoodComp foodComp)
        {
            if (foodComp.IsEaten == false)
            {
                for (int i = 0; i < creatureEntities.Length; i++)
                {
                    Entity creatureEntity = creatureEntities[i];
                    if (allCreatureComps[creatureEntity].IsDead == false)
                    {
                        Scale creatureScale = allScales[creatureEntity];
                        Translation creatureTranslation = allTranslations[creatureEntity];
                        float distance = Vector3.Distance(allTranslations[foodEntity].Value, creatureTranslation.Value);
                        if ((distance - creatureScale.Value) < creatureScale.Value)
                        {
                            foodComp.IsEaten = true;
                            CreatureComp creature = allCreatureComps[creatureEntity];
                            creature.Energy += math.clamp(creature.AbsorptionRate * foodComp.Energy, 0, creature.MaxEnergy);
                            if (creature.Energy > creature.IdleThreshold)
                            {
                                creature.IsIdle = true;
                            }
                            creature.FoodTarget = Entity.Null;
                            allCreatureComps[creatureEntity] = creature;
                            break;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        NativeArray<Entity> allCreatures = GetEntityQuery(typeof(CreatureComp)).ToEntityArray(Allocator.TempJob);
        ConsumeFoodJob job = new ConsumeFoodJob
        {
            creatureEntities = allCreatures,
            allTranslations = GetComponentDataFromEntity<Translation>(true),
            allScales = GetComponentDataFromEntity<Scale>(true),
            allCreatureComps = GetComponentDataFromEntity<CreatureComp>(false)
        };
        inputDeps = job.Schedule(this, inputDeps);
        return inputDeps;
    }
}

public class FoodTargetJobSystem : JobComponentSystem
{
    [BurstCompile]
    public struct TargetJob : IJobForEachWithEntity<CreatureComp>
    {
        [DeallocateOnJobCompletion]
        [ReadOnly] public NativeArray<Entity> foodEntities;
        [ReadOnly] public ComponentDataFromEntity<Translation> allTranslations;

        public void Execute(Entity creatureEntity, int index, ref CreatureComp creature)
        {
            if (creature.IsDead == true /*|| !creature.FoodTarget.Equals(Entity.Null)*/ || allTranslations.Exists(creature.FoodTarget))
            {
                return;
            }
            else
            {
                float shortestDistance = float.PositiveInfinity;
                float distance = 0;
                for (int i = 0; i < foodEntities.Length; i++)
                {
                    Entity foodEntity = foodEntities[i];
                    Translation foodTrans = allTranslations[foodEntity];
                    Translation creatureTrans = allTranslations[creatureEntity];
                    distance = Vector3.Distance(creatureTrans.Value, foodTrans.Value);
                    if (shortestDistance > distance)
                    {
                        shortestDistance = distance;
                        creature.FoodTarget = foodEntity;
                    }
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        NativeArray<Entity> entities = GetEntityQuery(typeof(FoodComp)).ToEntityArray(Allocator.TempJob);
        TargetJob job = new TargetJob()
        {
            foodEntities = entities,
            allTranslations = GetComponentDataFromEntity<Translation>(true)
        };
        inputDeps = job.Schedule(this, inputDeps);
        return inputDeps;
    }
}

public class MoveTowardsFoodJobSystem : JobComponentSystem
{
    [BurstCompile]
    public struct MoveJob : IJobForEachWithEntity<CreatureComp>
    {
        [ReadOnly] public float deltaTime;
        [NativeDisableParallelForRestriction] public ComponentDataFromEntity<Translation> allTranslations;

        public void Execute(Entity entity, int index, ref CreatureComp creature)
        {
            if (!allTranslations.Exists(creature.FoodTarget))
            {
                return;
            }
            else
            {
                if (creature.IsDead == true || creature.IsIdle == true || creature.FoodTarget.Equals(Entity.Null))
                {
                    return;
                }
                else
                {
                    float3 foodPos = allTranslations[creature.FoodTarget].Value;
                    Translation creaturePos = allTranslations[entity];
                    float3 moveDir = Vector3.Normalize(foodPos - creaturePos.Value);
                    creaturePos.Value += moveDir * creature.Speed * deltaTime;
                    allTranslations[entity] = creaturePos;
                }
            }
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        MoveJob job = new MoveJob
        {
            deltaTime = Time.smoothDeltaTime,
            allTranslations = GetComponentDataFromEntity<Translation>(false)
        };

        inputDeps = job.Schedule(this, inputDeps);
        return inputDeps;
    }
}