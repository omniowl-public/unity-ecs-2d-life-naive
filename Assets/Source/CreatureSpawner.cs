﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;

public class CreatureSpawner : MonoBehaviour
{
    private EntityManager m_EntityManager;
    private CommonResources resources;

    public void Awake()
    {
        m_EntityManager = World.Active.EntityManager;
        resources = Resources.Load<CommonResources>("CommonResources");
    }

    public void Start()
    {
        Unity.Mathematics.Random rnd = new Unity.Mathematics.Random();
        rnd.InitState();
        NativeArray<Entity> entityArray = new NativeArray<Entity>(resources.MaxCreatureCount, Allocator.Temp);
        EntityArchetype archeType = m_EntityManager.CreateArchetype(
            typeof(CreatureComp),
            typeof(RenderMesh),
            typeof(Translation),
            typeof(Rotation),
            typeof(Scale),
            typeof(LocalToWorld));
        m_EntityManager.CreateEntity(archeType, entityArray);

        // Declare outside of for-loop to save on allocation.
        float scale = 0;
        float energy = 0;
        float speed = resources.CreatureInfo.baseSpeed;
        float consumptionRate = 0;

        for (int i = 0; i < entityArray.Length; i++)
        {
            Entity entity = entityArray[i];

            m_EntityManager.SetComponentData(entity, new Translation()
            {
                Value = new float3(
                    rnd.NextFloat(resources.Boundaries.MinX, resources.Boundaries.MaxX),
                    0,
                    rnd.NextFloat(resources.Boundaries.MinZ, resources.Boundaries.MaxZ))
            });

            scale = rnd.NextFloat(resources.CreatureInfo.MinScale, resources.CreatureInfo.MaxScale);
            m_EntityManager.SetComponentData(entity, new Scale()
            {
                Value = scale
            });

            m_EntityManager.SetComponentData(entity, new Rotation()
            {
                Value = quaternion.EulerXYZ(90, 0, 0)
            });

            energy = rnd.NextFloat(resources.CreatureInfo.MinEnergy, resources.CreatureInfo.MaxEnergy);
            speed /= scale;
            consumptionRate = (scale * 1.25f) + (speed * 0.5f);

            m_EntityManager.SetComponentData(entity, new CreatureComp()
            {
                FoodTarget = Entity.Null,
                Birth = Time.time,
                Energy = energy,
                MaxEnergy = energy,
                AbsorptionRate = rnd.NextFloat(resources.CreatureInfo.MinAbsorptionRate, resources.CreatureInfo.MaxAbsorptionRate),
                ConsumptionRate = consumptionRate,
                IdleConsumptionRate = consumptionRate * 0.5f,
                IdleThreshold = rnd.NextFloat(resources.CreatureInfo.MinIdleThreshold, resources.CreatureInfo.MaxIdleThreshold),
                Speed = scale * 0.75f,
                LifeSpan = 0,
                IsIdle = true,
                IsDead = false
            });

            m_EntityManager.SetSharedComponentData(entity, new RenderMesh()
            {
                castShadows = ShadowCastingMode.Off,
                mesh = resources.CreatureMesh,
                material = resources.CreatureMaterial
            });
        }
        entityArray.Dispose();
    }
}
