﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[Serializable]
[CreateAssetMenu]
public class CommonResources : ScriptableObject
{
    [Serializable]
    public struct SpawnBoundaries
    {
        public float MinX;
        public float MaxX;
        public float MinZ;
        public float MaxZ;
    }

    [Serializable]
    public struct CreatureProperties
    {
        public float baseSpeed;
        public float MinScale;
        public float MaxScale;
        public float MinEnergy;
        public float MaxEnergy;
        [Range(0, 1)]
        public float MinAbsorptionRate;
        [Range(0, 1)]
        public float MaxAbsorptionRate;
        public float MinIdleThreshold;
        public float MaxIdleThreshold;
    }

    [SerializeField]
    public int MaxCreatureCount;
    [SerializeField]
    public int MaxFoodCount;
    [SerializeField]
    public SpawnBoundaries Boundaries;
    [SerializeField]
    public CreatureProperties CreatureInfo;
    [SerializeField]
    public Mesh CreatureMesh;
    [SerializeField]
    public Material CreatureMaterial;
    [SerializeField]
    public Mesh DeadCreatureMesh;
    [SerializeField]
    public Material DeadCreatureMaterial;
    [SerializeField]
    public Mesh FoodMesh;
    [SerializeField]
    public Material FoodMaterial;
}