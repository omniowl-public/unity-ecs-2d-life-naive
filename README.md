# Naive Unity ECS 2D Life Simulation

This simulation was run on the following specs:
- i7-4790K @4.00GHz(8 CPUs)
- 16GB RAM @ 2133Hz
- NVIDIA GTX 1070
- Unity 2019.1.0f2

**Below is a picture of the Profiler after it had run for a while. Your results may vary:**

![Profiler Results](https://i.imgur.com/eBNktjx.png)

**Video of the simulation:** https://www.youtube.com/watch?v=PPSQ7Knop_A

This is my first attempt at trying to make something in pure ECS in Unity. The FPS is averaging between 270 and 300, there are 250 creatures (white circles) on the screen at all times and 300 pieces of food (green squares) that gets refreshed. All of this can be controlled from the CommonResources asset file found in the Resources folder.

Whenever a circle turns red it means the creature died. The original creature entity is replaced with a dead version instead. Every creature is randomly generated in terms of size, speed, consumption rate of energy, absorption rate, etc.

Whenever food is passed over, it is marked as eaten and the creature that ate it gets energy back (according to its absorption rate and how much energy the food had).

All Creatures have a lazy approach in that they will always seek out the nearest piece of food and if it is eaten before they get there, they will look for a different piece of food.

All of the creatures have idle thresholds meaning that if the energy value is above a certain level, the creature will idle and consume less energy before finding more food.

Possible improvements:
- Better Documentation for the code itself
- Making sure that all parts of the simulation where it's feasible are using the Job System for optimal parallel processing.
- Making the spawning of new food smarter so that it can't spawn where a creature already is.
- Improving the intelligence of the agents so they can have more varied behaviour.
- Improve the visuals of each agent so they can be distinguished via colour or other visual features.